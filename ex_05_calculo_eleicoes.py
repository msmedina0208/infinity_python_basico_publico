###Objetivo, informar o % de cada tipo de voto contabilizado em relação ao total de votos do municipio 

print ('Vamos calcular a quantidade de cada tipo de votos')

quantidade_total_eleitores = 1000
quantidade_votos_validos = 750
quantidade_votos_brancos = 30
quantidade_votos_nulos = 120
quantidade_votos_nao_compareceu = 100


percentual_votos_validos = (quantidade_votos_validos/quantidade_total_eleitores)*100
percentual_votos_brancos = ( quantidade_votos_brancos/quantidade_total_eleitores)*100
percentual_votos_nulos = (quantidade_votos_nulos/quantidade_total_eleitores)*100
percentual_votos_nao_compareceu = (quantidade_votos_nao_compareceu/quantidade_total_eleitores)*100

print('percentual_votos_validos ', (percentual_votos_validos),'%')
print('percentual_votos_brancos ', (percentual_votos_brancos),'%')
print('percentual_votos_nulos ', (percentual_votos_nulos),'%')
print('percentual_votos_nao_compareceu ', (percentual_votos_nao_compareceu),'%')


