###O custo de um carro novo ao consumidor é a soma do custo de fábrica com a porcentagem do distribuidor e dos impostos (aplicados ao custo de fábrica). 
###Supondo que o percentual do distribuidor seja de 28%  e os impostos de 45%, escrever um algoritmo para ler o custo de fábrica de um carro, calcular e 
### escrever o custo final ao consumidor.

# CF = CUSTO DE FABRICA
# TAXA_DISTRIBUIDOR = 28%
# IMPOSTOS = 45%

# EXEMPLO:
# CF  = 100.000
# VALOR DOS IMPOSTOS = 100.000 * 45% = 45.000,00
# TAXA_DISTRIBUIDOR = 100.000 * 28% = 28.000,00

#RESULTADO ESPERADO = 173.000

print ('Vamos calcular o custo para o consumidor final do carro! Veja por quanto sai o custo de fabricação e por quanto fecha no cliente final')

CF = input ('informe o custo de fabricação do carro!')
CF = float(CF)

#Lembrando que a TAXA DO DISTRIBUIDOR é sobre o custo de fabricação
TAXA_DISTRIBUIDOR = 0.28
TAXA_DISTRIBUIDOR = float(TAXA_DISTRIBUIDOR)

VALOR_TAXA_DISTRIBUIDOR = (CF*TAXA_DISTRIBUIDOR)

#Lembrando que os impostos são calculados sobre o custo de produção!
TAXA_IMPOSTO = 0.45
TAXA_IMPOSTO = float(TAXA_IMPOSTO)

VALOR_IMPOSTO = (CF*TAXA_IMPOSTO)

#Valor total para o cliente final

VF = (CF+VALOR_TAXA_DISTRIBUIDOR+VALOR_IMPOSTO)

#mostrando na tela

print('Veja o detalhamento do valor para o consumidor final!')
print('Vc informou que o custo de produção foi ', CF)

print('O valor do imposto é de ', round(VALOR_IMPOSTO,2))

print ('O valor da taxa do distribuidor é ', round(VALOR_TAXA_DISTRIBUIDOR, 2))

print('Custo para o cliente final ', VF)